#!/bin/bash

x="teste"
menu ()
{
while true $x != "teste"
do
clear
echo "------------------------------------------------"
echo "|           Criado por: Ramon Santos.          |"
echo "------------------------------------------------" 
echo 
echo "==================== MENU ======================"
echo""
echo "1)Informação do Processador."
echo""
echo "2)Informação da Memória."
echo""
echo "3)Informação do HD/SSD."
echo""
echo "4)Verificar se o diretório existe."
echo""
echo "5)Exibir o diretório atual."
echo""
echo "6)Listar arquivos do diretorio atual."
echo""
echo "7)Mudar o diretório atual."
echo""
echo "8)Sair do programa."
echo""
echo "================================================"

echo "Digite a opção desejada:"
read x
echo "Opção informada ($x)"
echo "================================================"

case "$x" in


    1)
      echo "Informações da CPU:"
      lscpu | grep name:
      lscpu | grep family:
      sleep 8

echo "================================================"
;;
    2)
      echo "Informações de memória:"
      free -m
      sleep 8

echo "================================================"
;;
   3)
      echo "Informação do HDD/SSD:"
      lsblk
      sleep 8

echo "================================================"
;;
    4)
       echo "Verificando o diretorio..."
       read -p "Diretório:" d1
       ls $d1 | wc -l
       echo "FIM"
       sleep 5

echo "================================================"
;;
     5)
       echo "Exibindo diretorio atual..."
       sleep 3
       pwd
       sleep 3

echo "================================================"
;;
    6)
    echo "Listando arquivos..."
    sleep 3
    ls
    sleep 5

echo "================================================"
;;
    7)
    echo -e "\033[0;32mDiretórios disponíveis: \033[0m" 
    ls
    echo -e "\033[0;31mVocê está em: \033[0m"
    pwd
    echo "Digite o diretório que deseja acessar:"
    read dir
    cd $dir
    echo "Agora vc acessou o diretório:"
    pwd
    sleep 5 

echo "================================================"
 ;;
       8)
         echo "saindo..."
         sleep 2
         clear;
         exit;
echo "================================================"
;;

*)
        echo "Opção inválida!"
esac
done

}
menu
	
